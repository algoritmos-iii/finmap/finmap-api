## INCLUI REGISTROS NAS TABELAS

## --------------- TABELA USUARIO ---------------
INSERT INTO usuario (id, nome, sobrenome, email, senha)
VALUES (1, "Paulo", "Henrique", "paulo@gmail.com", "senha123");

INSERT INTO usuario (id, nome, sobrenome, email, senha)
VALUES (2, "Vitor", "Hugo", "vitor@gmail.com", "senha456");

## --------------- TABELA TIPO ---------------
INSERT INTO tipo (id, descricao)
VALUES (1, "Comida");

INSERT INTO tipo (id, descricao)
VALUES (2, "Trabalho");

# --------------- TABELA SALDO ---------------
INSERT INTO saldo (id, mes, ano, valor, usuario_id)
VALUES (1, 3, 2019, 200.55, 1);

INSERT INTO saldo (id, mes, ano, valor, usuario_id)
VALUES (2, 2, 2019, 514.25, 1);

INSERT INTO saldo (id, mes, ano, valor, usuario_id)
VALUES (3, 3, 2019, -20.45, 2);

## --------------- TABELA META ---------------
INSERT INTO meta (id, titulo, descricao, valor_atual, valor_total, data_inicial, data_final, usuario_id)
VALUES (1,
        "Viajar",
        "Quero viajar no final do ano e para isso tenho que juntar dinheiro desde agora.",
        120.00,
        2500.00,
        "2019-02-05",
        "2019-12-05",
        1);

INSERT INTO meta (id, titulo, descricao, valor_atual, valor_total, data_inicial, data_final, usuario_id)
VALUES (2,
        "Carro Novo",
        "Quero trocar de carro mas não tenho dinheiro agora, então vou juntar pra comprar a vista em 2020.",
        2900.00,
        19000.00,
        "2019-01-01",
        "2020-01-01",
        1);

## --------------- TABELA CONTA ---------------
INSERT INTO conta (id, descricao, num_parcelas, usuario_id)
VALUES (1, "Fatura do Cartão", 1, 1);

INSERT INTO conta (id, descricao, num_parcelas, usuario_id)
VALUES (2, "Computador Novo", 3, 1);

## --------------- TABELA PARCELA ---------------
INSERT INTO parcela (id, numero, vencimento, valor, status, conta_id)
VALUES (1, 1, "2019-03-09", 90.00, TRUE, 1);

INSERT INTO parcela (id, numero, vencimento, valor, status, conta_id)
VALUES (2, 1, "2019-01-25", 1500.00, TRUE, 2);

INSERT INTO parcela (id, numero, vencimento, valor, status, conta_id)
VALUES (3, 2, "2019-02-25", 1500.00, FALSE, 2);

INSERT INTO parcela (id, numero, vencimento, valor, status, conta_id)
VALUES (4, 3, "2019-03-25", 1500.00, FALSE, 2);

## --------------- TABELA LANCAMENTO ---------------
INSERT INTO lancamento (id, data, descricao, valor, tipo_id, usuario_id)
VALUES (1, "2019-03-09", "Pastel", -4.00, 1, 1);

INSERT INTO lancamento (id, data, descricao, valor, tipo_id, usuario_id)
VALUES (2, "2019-03-11", "Salario do mês", 1000.00, 2, 1);