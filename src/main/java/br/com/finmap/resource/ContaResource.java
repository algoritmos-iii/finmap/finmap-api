package br.com.finmap.resource;

import br.com.finmap.domain.Conta;
import br.com.finmap.repository.ContaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/conta")
public class ContaResource {

    @Autowired
    ContaRepository contaRepository;

    @GetMapping
    public List<Conta> listAll() {
        return contaRepository.findAll();
    }

    @GetMapping("/{id}")
    public Conta findOne(@PathVariable int id){
        return contaRepository.findById(id).get();
    }

    @PostMapping
    public ResponseEntity<Conta> save(@Valid @RequestBody Conta conta) {
        Conta bill = contaRepository.save(conta);
        return new ResponseEntity<Conta>(bill, HttpStatus.CREATED);
    }

    @PutMapping
    public ResponseEntity<Conta> edit(@Valid @RequestBody Conta conta) {
        Conta bill = contaRepository.save(conta);
        return new ResponseEntity<Conta>(bill, HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Conta> remove(@PathVariable int id) {
        Conta bill = contaRepository.findById(id).get();
        contaRepository.delete(bill);
        return new ResponseEntity<Conta>(HttpStatus.NO_CONTENT);
    }
}
