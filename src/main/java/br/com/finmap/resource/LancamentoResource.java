package br.com.finmap.resource;

import br.com.finmap.domain.Lancamento;
import br.com.finmap.repository.LancamentoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/lancamento")
public class LancamentoResource {

    @Autowired
    LancamentoRepository lancamentoRepository;

    @GetMapping
    public List<Lancamento> listAll() {
        return lancamentoRepository.findAll();
    }

    @GetMapping("/{id}")
    public Lancamento findOne(@PathVariable int id){
        return lancamentoRepository.findById(id).get();
    }

    @PostMapping
    public ResponseEntity<Lancamento> save(@Valid @RequestBody Lancamento lancamento) {
        Lancamento release = lancamentoRepository.save(lancamento);
        return new ResponseEntity<Lancamento>(release, HttpStatus.CREATED);
    }

    @PutMapping
    public ResponseEntity<Lancamento> edit(@Valid @RequestBody Lancamento lancamento) {
        Lancamento release = lancamentoRepository.save(lancamento);
        return new ResponseEntity<Lancamento>(release, HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Lancamento> remove(@PathVariable int id) {
        Lancamento release = lancamentoRepository.findById(id).get();
        lancamentoRepository.delete(release);
        return new ResponseEntity<Lancamento>(HttpStatus.NO_CONTENT);
    }
}
