package br.com.finmap.resource;

import br.com.finmap.domain.Saldo;
import br.com.finmap.repository.SaldoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/saldo")
public class SaldoResource {

    @Autowired
    SaldoRepository saldoRepository;

    @GetMapping
    public List<Saldo> listAll() {
        return saldoRepository.findAll();
    }

    @GetMapping("/{id}")
    public Saldo findOne(@PathVariable int id){
        return saldoRepository.findById(id).get();
    }

    @PostMapping
    public ResponseEntity<Saldo> save(@Valid @RequestBody Saldo saldo) {
        Saldo leftover = saldoRepository.save(saldo);
        return new ResponseEntity<Saldo>(leftover, HttpStatus.CREATED);
    }

    @PutMapping
    public ResponseEntity<Saldo> edit(@Valid @RequestBody Saldo saldo) {
        Saldo leftover = saldoRepository.save(saldo);
        return new ResponseEntity<Saldo>(leftover, HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Saldo> remove(@PathVariable int id) {
        Saldo leftover = saldoRepository.findById(id).get();
        saldoRepository.delete(leftover);
        return new ResponseEntity<Saldo>(HttpStatus.NO_CONTENT);
    }
}
