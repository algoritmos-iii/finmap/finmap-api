package br.com.finmap.resource;

import br.com.finmap.domain.Tipo;
import br.com.finmap.repository.TipoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/tipo")
public class TipoResource {

    @Autowired
    TipoRepository tipoRepository;

    @GetMapping
    public List<Tipo> listAll() {
        return tipoRepository.findAll();
    }

    @GetMapping("/{id}")
    public Tipo findOne(@PathVariable int id){
        return tipoRepository.findById(id).get();
    }

    @PostMapping
    public ResponseEntity<Tipo> save(@Valid @RequestBody Tipo tipo) {
        Tipo type = tipoRepository.save(tipo);
        return new ResponseEntity<Tipo>(type, HttpStatus.CREATED);
    }

    @PutMapping
    public ResponseEntity<Tipo> edit(@Valid @RequestBody Tipo tipo) {
        Tipo type = tipoRepository.save(tipo);
        return new ResponseEntity<Tipo>(type, HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Tipo> remove(@PathVariable int id) {
        Tipo type = tipoRepository.findById(id).get();
        tipoRepository.delete(type);
        return new ResponseEntity<Tipo>(HttpStatus.NO_CONTENT);
    }
}
