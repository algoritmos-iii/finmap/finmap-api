package br.com.finmap.resource;

import br.com.finmap.domain.Usuario;
import br.com.finmap.repository.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/usuario")
public class UsuarioResource {

    @Autowired
    UsuarioRepository usuarioRepository;

    @GetMapping
    public List<Usuario> listAll() { return usuarioRepository.findAll(); }

    @GetMapping("/{id}")
    public Usuario findOne(@PathVariable int id){ return usuarioRepository.findById(id).get(); }

    @PostMapping
    public ResponseEntity<Usuario> save(@Valid @RequestBody Usuario usuario) {
        Usuario user = usuarioRepository.save(usuario);
        return new ResponseEntity<Usuario>(user, HttpStatus.CREATED);
    }

    @PutMapping
    public ResponseEntity<Usuario> edit(@Valid @RequestBody Usuario usuario) {
        Usuario user = usuarioRepository.save(usuario);
        return new ResponseEntity<Usuario>(user, HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Usuario> remove(@PathVariable int id) {
        Usuario user = usuarioRepository.findById(id).get();
        usuarioRepository.delete(user);
        return new ResponseEntity<Usuario>(HttpStatus.NO_CONTENT);
    }
}
