package br.com.finmap.resource;

import br.com.finmap.domain.Parcela;
import br.com.finmap.repository.ParcelaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/parcela")
public class ParcelaResource {

    @Autowired
    ParcelaRepository parcelaRepository;

    @GetMapping
    public List<Parcela> listAll() {
        return parcelaRepository.findAll();
    }

    @GetMapping("/{id}")
    public Parcela findOne(@PathVariable int id){
        return parcelaRepository.findById(id).get();
    }

    @PostMapping
    public ResponseEntity<Parcela> save(@Valid @RequestBody Parcela parcela) {
        Parcela portion = parcelaRepository.save(parcela);
        return new ResponseEntity<Parcela>(portion, HttpStatus.CREATED);
    }

    @PutMapping
    public ResponseEntity<Parcela> edit(@Valid @RequestBody Parcela parcela) {
        Parcela portion = parcelaRepository.save(parcela);
        return new ResponseEntity<Parcela>(portion, HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Parcela> remove(@PathVariable int id) {
        Parcela portion = parcelaRepository.findById(id).get();
        parcelaRepository.delete(portion);
        return new ResponseEntity<Parcela>(HttpStatus.NO_CONTENT);
    }
}
