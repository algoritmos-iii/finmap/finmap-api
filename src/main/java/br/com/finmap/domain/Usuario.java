package br.com.finmap.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import java.util.Set;

@Entity
public @Data class Usuario {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @NotBlank(message = "O nome não pode ser nulo")
    @Column(nullable = false, length = 50)
    private String nome;

    @NotBlank(message = "O sobrenome não pode ser nulo")
    @Column(nullable = false, length = 50)
    private String sobrenome;

    @Email
    @NotBlank(message = "O email não pode ser nulo")
    @Column(nullable = false, length = 50)
    private String email;

    @NotBlank(message = "A sena não pode ser nulo")
    @Column(nullable = false, length = 32)
    private String senha;

    @OneToMany(mappedBy = "usuario")
    @JsonIgnore
    private Set<Saldo> saldos;

    @JsonIgnore
    @OneToMany(mappedBy = "usuario")
    private Set<Lancamento> lancamentos;

    @JsonIgnore
    @OneToMany(mappedBy = "usuario")
    private Set<Meta> metas;

    @JsonIgnore
    @OneToMany(mappedBy = "usuario")
    private Set<Conta> contas;

}
