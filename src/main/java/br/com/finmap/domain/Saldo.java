package br.com.finmap.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.*;

@Entity
public @Data class Saldo {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @NotNull(message = "O valor não pode ser nulo")
    @Digits(integer=8, fraction=2, message = "O valor é inválido")
    @Column(nullable = false, precision=8, scale=2)
    private float valor;

    @Max(value = 12, message = "Valor máximo do mês é 12")
    @Min(value = 1, message = "Valor mínimo do mês é 1")
    @NotNull(message = "O mês não pode ser nulo")
    @Column(nullable = false, length = 2)
    private int mes;

    @NotEmpty(message = "O ano não pode ser zero ou nulo")
    @Column(nullable = false, length = 4)
    private int ano;

    @ManyToOne(cascade = CascadeType.PERSIST)
    private @Valid Usuario usuario;

}
