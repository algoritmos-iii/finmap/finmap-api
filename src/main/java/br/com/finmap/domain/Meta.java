package br.com.finmap.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.Digits;
import javax.validation.constraints.Future;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.Set;

@Entity
public @Data class Meta {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @NotBlank(message = "O título não pode ser nulo")
    @Column(nullable = false, length = 100)
    private String titulo;

    @Column(length = 500)
    private String descricao;

    @NotNull(message = "O valorAtual não pode ser nulo")
    @Digits(integer=8, fraction=2, message = "O valor é inválido")
    @Column(nullable = false, precision=8, scale=2)
    private float valorAtual;

    @NotNull(message = "O valorTotal não pode ser nulo")
    @Digits(integer=8, fraction=2, message = "O valor é inválido")
    @Column(nullable = false, precision=8, scale=2)
    private float valorTotal;

    @NotBlank(message = "A dataInicial não pode ser nulo")
    @Column(nullable = false)
    private LocalDate dataInicial;

    @Future(message = "A dataFinal é inválida, precisa ser uma data futura")
    @NotBlank(message = "A dataFinal não pode ser nulo")
    @Column(nullable = false)
    private LocalDate dataFinal;

    @ManyToOne(cascade = CascadeType.PERSIST)
    private @Valid Usuario usuario;

    @JsonIgnore
    @OneToMany(mappedBy = "meta")
    private Set<Lancamento> lancamentos;
}
