package br.com.finmap.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.util.Set;

@Entity
public @Data class Tipo {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @NotBlank(message = "A descricao não pode ser nulo")
    @Column(nullable = false, length = 50)
    private String descricao;

    @JsonIgnore
    @OneToMany(mappedBy = "tipo")
    private Set<Lancamento> lancamentos;

}
